import java.util.*;

public class Tnode {

   private String name;
   private Tnode firstChild;
   private Tnode nextSibling;

   public Tnode(String name) {
      this.name = name;
   }

   public String getName() {
      return this.name;
   }

   public void setName(String name) { this.name = name; }

   public Tnode getNextSibling() {
      return this.nextSibling;
   }

   public void setFirstChild(Tnode child) {
      this.firstChild = child;
   }

   public void setNextSibling(Tnode sibling) {
      this.nextSibling = sibling;
   }

   public Tnode clone() {
      Tnode newTnode = new Tnode(name);

      if (firstChild != null) {
         newTnode.firstChild = firstChild.clone();
      } else if (nextSibling != null) {
         newTnode.nextSibling = nextSibling.clone();
      }
      return newTnode;
   }

   @Override
   public String toString() {
      StringBuffer b = new StringBuffer();
      if (isOperator(name)) {
         b.append(name);
         b.append("(");
         b.append(firstChild.toString());
         b.append(")");

         if (nextSibling != null) {
            b.append(",");
            b.append(nextSibling);
         }
      } else {
         b.append(name);

         if (nextSibling != null) {
            b.append(",");
            b.append(nextSibling);
         }
      }

      return b.toString();
   }

   public static Tnode buildFromRPN (String pol) {
      Tnode root = null;
      Tnode rightSide = null;

      for (String character : pol.split("\\s+")) {
         if (!isValidSymbol(character)) {
            throw new RuntimeException(character + " is not a valid symbol. Provided equation: " + pol);
         }

         if (root == null) {
            if (isOperator(character) || isSpecialOperator(character)) {
               throw new RuntimeException("Can't apply operator " + character + " because there are no elements. Provided equation: " + pol);
            }

            root = new Tnode(character);
         } else if (isSpecialOperator(character)) {
            switch (character) {
               case "DUP":
                  if (rightSide != null) {
                     rightSide.setNextSibling(rightSide.clone());
                  } else if (root.getNextSibling() != null) {
                     rightSide = root.getNextSibling();
                     rightSide.setNextSibling(rightSide.clone());
                     root.setNextSibling(null);
                  } else {
                     rightSide = root.clone();
                  }
                  break;
               case "SWAP":
                  Tnode newRoot;

                  if (rightSide != null) {
                     newRoot = rightSide;
                     rightSide = root;
                  } else if (root.getNextSibling() != null) {
                     newRoot = root.getNextSibling();
                     root.setNextSibling(null);
                     newRoot.setNextSibling(root);
                  } else {
                     throw new RuntimeException("Can't apply operator " + character + " because there is only 1 element in stack. Provided equation: " + pol);
                  }

                  root = newRoot;
                  break;
               case "ROT":
                  if (rightSide == null || rightSide.getNextSibling() == null) {
                     throw new RuntimeException("Can't apply operator " + character + " because there are not enough elements in stack. Provided equation: " + pol);
                  }

                  rightSide.getNextSibling().setNextSibling(root);
                  root = rightSide;
                  rightSide = root.getNextSibling();
                  root.setNextSibling(null);

                  break;
            }
         } else if (isOperator(character)) {
            if (rightSide == null && root.getNextSibling() == null) {
               throw new RuntimeException("Can't apply operator " + character + " because there is only one element. Provided equation: " + pol);
            }

            if (rightSide != null) {
               if (rightSide.getNextSibling() != null || (isOperator(rightSide.getName()) && !isOperator(root.getName()))) {
                  Tnode newTnode = new Tnode(character);
                  newTnode.setFirstChild(rightSide);
                  root.setNextSibling(newTnode);
                  rightSide = null;
                  continue;
               }
               root.setNextSibling(rightSide);
               rightSide = null;
            }

            Tnode newTnode = new Tnode(character);
            newTnode.setFirstChild(root);
            root = newTnode;
         } else if (rightSide != null){
            rightSide.setNextSibling(new Tnode(character));
         } else {
            rightSide = new Tnode(character);
         }
      }

      if (rightSide != null) {
         throw new RuntimeException("Not enough operators to finish the equation. Provided equation: " + pol);
      }

      return root;
   }

   private static boolean isOperator(String element) {
      List<String> operators = new ArrayList<>();
      operators.add("+");
      operators.add("-");
      operators.add("*");
      operators.add("/");

      return operators.contains(element);
   }

   private static boolean isSpecialOperator(String element) {
      List<String> operators = new ArrayList<>();
      operators.add("DUP");
      operators.add("SWAP");
      operators.add("ROT");

      return operators.contains(element);
   }

   private static boolean isValidSymbol(String character) {
      if (isOperator(character) || isSpecialOperator(character)) {
         return true;
      }

      try {
         Double.parseDouble(character);
         return true;
      } catch (NumberFormatException exception) {
         return false;
      }
   }

   public static void main (String[] param) {
      String rpn = "1 2 +";
      System.out.println ("RPN: " + rpn);
      Tnode res = buildFromRPN (rpn);
      System.out.println ("Tree: " + res);
      // TODO!!! Your tests here
   }

}

/*
Täiendage meetodit buildFromRPN  nii, et see aktsepteeriks sisendis veel operatsioone DUP, SWAP ja ROT, millel on järgmine tähendus:
DUP loob viimasest alampuust koopia (kontrollida, et puu leidub)
SWAP vahetab kaks viimast alampuud (kontrollida, et leidub kaks alampuud)
ROT roteerib kolme viimast alampuud, tõstes kolmanda esimeseks (kontrollida, et leidub kolm alampuud)
Vaadake nende operatsioonide tähendust kolmanda kodutöö kaitsmise juhendist - puu väljaarvutamisel peab tulemus olema sama, mille väljastaks sama sisendi korral meetod interpret.
Näited:
"2 5 SWAP -"  peab vahetama alampuud 2 ja 5 ning andma puu   -(5,2)
"3 DUP *" peab kopeerima puu 3 ning andma puu  *(3,3)
"2 5 9 ROT - +"  peab tõstma puu 2 esimeseks ning andma puu   +(5,-(9,2))
"2 5 9 ROT + SWAP -"  peab andma puu   -(+(9,2),5)
 "2 5 DUP ROT - + DUP *" peab andma puu  *(+(5,-(5,2)),+(5,-(5,2)))
"-3 -5 -7 ROT - SWAP DUP * +" peab andma puu  +(-(-7,-3),*(-5,-5))
Realiseerige muudatused meetodis buildFromRPN  ja testige tulemus automaattestidega. Kindlasti proovige eelnevaid näiteid.
*/